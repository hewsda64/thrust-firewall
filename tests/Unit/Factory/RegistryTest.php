<?php

declare(strict_types=1);

namespace ThrustTest\Firewall\Unit\Factory;

use PHPUnit\Framework\TestCase;
use Thrust\Firewall\Factory\Registry;

class RegistryTest extends TestCase
{
    /**
     * @test
     */
    public function it_return_bootstraps(): void
    {
        $bootstrap = ['foo', 1];

        $bootstraps = new Registry([$bootstrap]);

        $this->assertCount(1, $bootstraps->bootstraps());
    }

    /**
     * @test
     */
    public function it_add_bootstrap(): void
    {
        $bootstraps = new Registry();

        $this->assertCount(0, $bootstraps->bootstraps());

        $bootstraps->append('foo', 1);

        $this->assertCount(1, $bootstraps->bootstraps());
    }

    /**
     * @test
     */
    public function it_prioritize_bootstraps(): void
    {
        $bootstrap = [['foo', 100], ['bar', 20]];

        $bootstraps = new Registry($bootstrap);

        $this->assertEquals(['bar', 'foo'], $bootstraps->bootstraps());
    }

    /**
     * @test
     */
    public function it_prioritize_bootstraps_2(): void
    {
        $bootstrap = [['foo', 100], 'bar'];

        $bootstraps = new Registry($bootstrap);

        $this->assertEquals(['bar', 'foo'], $bootstraps->bootstraps());
    }

    /**
     * @test
     */
    public function it_return_highest_priority(): void
    {
        $bootstraps = new Registry();
        $bootstraps->append('foo', 10);

        $this->assertEquals(10, $bootstraps->getHighestPriority());

        $bootstraps->append('bar', 100);

        $this->assertEquals(100, $bootstraps->getHighestPriority());
    }

    /**
     * @test
     * @expectedException \Thrust\Firewall\Exception\FirewallException
     */
    public function it_raise_exception_when_service_is_not_unique(): void
    {
        $this->expectExceptionMessage('Firewall bootstrap service "foo" already exists.');

        $bootstrap = ['foo', 'foo'];

        new Registry($bootstrap);
    }

    /**
     * @test
     * @expectedException \Thrust\Firewall\Exception\FirewallException
     */
    public function it_raise_exception_when_adding_service_is_not_formatted(): void
    {
        $this->expectExceptionMessage('Bootstrap service must be an array of two members with service id and priority');

        new Registry([['foo']]);
    }

    /**
     * @test
     * @expectedException \Thrust\Firewall\Exception\FirewallException
     */
    public function it_raise_exception_when_priority_is_not_set(): void
    {
        $this->expectExceptionMessage('No priority set to service bootstrap "foo"');

        new Registry([['foo', 'bar']]);
    }
}