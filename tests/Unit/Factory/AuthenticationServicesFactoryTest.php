<?php

declare(strict_types=1);

namespace ThrustTest\Firewall\Unit\Factory;

use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Thrust\Firewall\Factory\AuthenticationServices;
use Thrust\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;

class AuthenticationServicesFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_return_a_collection(): void
    {
        $services = new AuthenticationServices(null);

        $this->assertInstanceOf(Collection::class, $services->all());
    }

    /**
     * @test
     */
    public function it_can_add_service(): void
    {
        $services = new AuthenticationServices();

        $this->assertCount(0, $services->all());

        $service = $this->getMockForAbstractClass(AuthenticationServiceFactory::class);

        $services->add($service);

        $this->assertCount(1, $services->all());
    }
}