<?php

declare(strict_types=1);

namespace ThrustTest\Firewall\Unit\Factory;

use PHPUnit\Framework\TestCase;
use Thrust\Firewall\Factory\Aggregate;
use Thrust\Firewall\Factory\Payload\PayloadFactory;

class AggregateTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_return_listeners(): void
    {
        $ag = new Aggregate();
        $this->assertCount(0, $ag->listeners());

        $payload = $this->getMockBuilder(PayloadFactory::class)->disableOriginalConstructor()->getMock();
        $payload->expects($this->atLeastOnce())->method('listener')->willReturn('foo');

        $ag($payload);

        $this->assertCount(1, $ag->listeners());
    }

    /**
     * @test
     */
    public function it_can_return_providers(): void
    {
        $ag = new Aggregate();
        $this->assertCount(0, $ag->providers());

        $payload = $this->getMockBuilder(PayloadFactory::class)->disableOriginalConstructor()->getMock();
        $payload->expects($this->atLeastOnce())->method('provider')->willReturn('foo');

        $ag($payload);

        $this->assertCount(1, $ag->providers());
    }

    /**
     * @test
     */
    public function it_can_return_entrypoints(): void
    {
        $ag = new Aggregate();
        $this->assertCount(0, $ag->entrypoints());

        $payload = $this->getMockBuilder(PayloadFactory::class)->disableOriginalConstructor()->getMock();
        $payload->expects($this->atLeastOnce())->method('entrypoint')->willReturn('foo');

        $ag($payload);

        $this->assertCount(1, $ag->entrypoints());
    }
}