<?php

declare(strict_types=1);

namespace Thrust\Firewall;

use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;
use Thrust\Firewall\Factory\Factory;
use Thrust\Firewall\Factory\Registry;

class Director
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Pipeline
     */
    private $pipeline;

    /**
     * Director constructor.
     *
     * @param Registry $registry
     * @param Pipeline $pipeline
     */
    public function __construct(Registry $registry, Pipeline $pipeline)
    {
        $this->registry = $registry;
        $this->pipeline = $pipeline;
    }

    public function process(Collection $authenticationServices, Request $request): Collection
    {
        return $authenticationServices->map(function (Factory $factory) use ($request) {
            return $this->pipeline
                ->via('compose')
                ->through($this->registry->bootstraps())
                ->send($factory->setRequest($request))
                ->then(function () use ($factory) {
                    return $factory->middleware();
                });
        });
    }
}