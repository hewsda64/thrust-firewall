<?php

use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Authorization\Authorizable;
use Thrust\Security\Contract\Authorization\Grantable;
use Thrust\Security\Contract\Token\Storage\TokenStorage;
use Thrust\Security\Contract\Token\Tokenable;

if (!function_exists('getToken')) {

    function getToken(): ?Tokenable
    {
        return app(TokenStorage::class)->getToken();
    }
}

if (!function_exists('getUser')) {

    /**
     * @return \Thrust\Security\Contract\User\User|\Thrust\Security\Contract\Value\Identifier
     */
    function getUser()
    {
        return app(TokenStorage::class)->getToken()->user();
    }
}

if (!function_exists('isAnonymous')) {

    function isAnonymous(Tokenable $token = null): bool
    {
        return getTrustResolver()->isAnonymous($token);
    }
}

if (!function_exists('isFullFledged')) {

    function isFullFledged(Tokenable $token = null): bool
    {
        return getTrustResolver()->isFullFledged($token);
    }
}

if (!function_exists('isRememberMe')) {

    function isRememberMe(Tokenable $token = null): bool
    {
        return getTrustResolver()->isRememberMe($token);
    }
}

if (!function_exists('hasRole')) {

    function hasRole(Tokenable $token, string $role, $object = null): bool
    {
        return getAuthorizer()->decide($token, [$role], $object ?? request());
    }
}

if (!function_exists('hasRoles')) {

    function hasRoles(Tokenable $token, array $roles, $object = null): bool
    {
        return getAuthorizer()->decide($token, $roles, $object ?? request());
    }
}

if (!function_exists('getAuthorizer')) {

    function getAuthorizer(): Authorizable
    {
        return app(Authorizable::class);
    }
}

if (!function_exists('isGranted')) {

    function isGranted($attribute, $subject = null): bool
    {
        return app(Grantable::class)->isGranted((array)$attribute, $subject);
    }
}

if (!function_exists('isNotGranted')) {

    function isNotGranted($attribute, $subject = null): bool
    {
        return !isGranted($attribute, $subject);
    }
}

if (!function_exists('getTrustResolver')) {

    function getTrustResolver(): TrustResolver
    {
        return app(TrustResolver::class);
    }
}



