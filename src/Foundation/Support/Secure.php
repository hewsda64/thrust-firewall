<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Support;

use Thrust\Security\Contract\Authorization\Grantable;
use Thrust\Security\Contract\Exception\AuthorizationException;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Exception\AuthenticationCredentialsNotFound;
use Thrust\Security\Foundation\Guard;

trait Secure
{
    use Guard;

    protected function grant(): Grantable
    {
        return app(Grantable::class);
    }

    protected function isGranted($attributes, $object = null): bool
    {
        return $this->grant()->isGranted((array)$attributes, $object);
    }

    protected function requireGranted($attributes, $object = null): bool
    {
        if (!$this->isGranted($attributes, $object)) {
            $this->raiseAuthorizationDenied();
        }

        return true;
    }

    /**
     * @return User|Identifier
     *
     * @throws AuthenticationCredentialsNotFound
     */
    protected function user()
    {
        if (!$this->hasToken()) {
            throw new AuthenticationCredentialsNotFound('No token found in storage.');
        }

        return $this->token()->user();
    }

    protected function identifyUser(): string
    {
        $this->requireGranted('ROLE_USER');

        return $this->user()->getId()->identify();
    }

    protected function raiseAuthorizationDenied(string $message = null): void
    {
        $message = $message ?? 'Authorization denied.';

        throw new AuthorizationException($message);
    }
}