<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Http\Middleware;

use Illuminate\Http\Request;
use Thrust\Security\Foundation\Authorizer;
use Thrust\Security\Foundation\Guard;

class Authorization
{
    use Guard, Authorizer;

    public function handle(Request $request, \Closure $next, ...$attributes)
    {
        $token = $this->requireToken();

        if (!$token->isAuthenticated()) {
            $this->setToken($token = $this->authenticate($token));
        }

        $this->requireGranted($token, $attributes, $request);

        return $next($request);
    }
}