<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Http\Middleware;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\TerminableInterface;
use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Event\ContextEventable;
use Thrust\Security\Foundation\HasToken;

class SessionContextAware implements TerminableInterface
{
    use HasToken;

    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var ContextEventable
     */
    private $eventContext;

    /**
     * FirewallContextAware constructor.
     *
     * @param Dispatcher $events
     * @param TrustResolver $trustResolver
     */
    public function __construct(Dispatcher $events, TrustResolver $trustResolver)
    {
        $this->events = $events;
        $this->trustResolver = $trustResolver;
    }

    public function handle(Request $request, \Closure $next)
    {
        $this->events->listen(ContextEventable::class, [$this, 'onEvent']);

        return $next($request);
    }

    public function onEvent(ContextEventable $event): void
    {
        if (null !== $this->eventContext) {
            throw new \RuntimeException('Only one context can run per request.');
        }

        $this->eventContext = $event;
    }

    public function terminate(SymfonyRequest $request, Response $response): void
    {
        if ($this->eventContext) {

            $token = $this->token();

            if (null === $token || $this->trustResolver->isAnonymous($token)
            ) {
                $request->session()->forget($this->eventContext->sessionKey());
            } else {
                $request->session()->put($this->eventContext->sessionKey(), serialize($token));
            }
        }
    }
}