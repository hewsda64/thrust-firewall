<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Http\Middleware;

use Illuminate\Http\Request;
use Thrust\Firewall\Foundation\Contracts\Strategy\FirewallStrategy;

class Firewall
{
    /**
     * @var FirewallStrategy
     */
    private $strategy;

    /**
     * Firewall constructor.
     *
     * @param FirewallStrategy $strategy
     */
    public function __construct(FirewallStrategy $strategy)
    {
        $this->strategy = $strategy;
    }
    public function handle(Request $request, \Closure $next)
    {
        $this->strategy->delegateHandling($request);

        return $next($request);
    }
}