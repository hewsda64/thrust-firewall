<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Http\Request;

use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;
use Thrust\Security\Authentication\Token\Value\SimpleCredentials;
use Thrust\Security\Contract\Http\Request\AuthenticationRequest;
use Thrust\Security\User\Value\EmailAddress;

class HttpBasicAuthenticationRequest implements AuthenticationRequest
{

    public function extract(IlluminateRequest $request): array
    {
        return [
            EmailAddress::fromString($request->headers->get('PHP_AUTH_USER')),
            SimpleCredentials::fromString($request->headers->get('PHP_AUTH_PW'))
        ];
    }

    public function matches(Request $request): bool
    {
        return null === $request->headers->get('PHP_AUTH_USER');
    }
}