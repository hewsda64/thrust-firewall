<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Bootstrap;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Thrust\Firewall\Factory\Factory;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Security\Foundation\Value\ContextKey;
use Thrust\Security\Request\Firewall\ContextFirewall;

class AuthenticationSerialization
{
    /**
     * @var Container
     */
    private $container;

    /**
     * AuthenticationSerialization constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        if (!$factory->context()->isStateless()) {
            $listenerId = 'firewall.context.default_context_listener.' . $factory->key();

            $this->registerListener($listenerId, $factory);

            $factory((new PayloadFactory())->setListener($listenerId));
        }

        return $make($factory);
    }

    private function registerListener(string $listenerId, Factory $factory): void
    {
        $this->container->bind($listenerId, function (Application $app) use ($factory) {

            $providers = array_flatten($factory->userProviderIds());

            foreach ($providers as &$provider) {
                $provider = $this->container->make($provider);
            }

            return new ContextFirewall(new ContextKey($factory->key()), $app->make('events'), $providers);
        });
    }
}