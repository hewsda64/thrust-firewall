<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Bootstrap;

use Illuminate\Contracts\Container\Container;
use Thrust\Firewall\Factory\Factory;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Security\Authentication\Provider\AnonymousAuthenticationProvider;
use Thrust\Security\Foundation\Value\AnonymousKey;
use Thrust\Security\Request\Firewall\AnonymousFirewall;

class AuthorizeAnonymousRequest
{
    /**
     * @var Container
     */
    private $container;

    /**
     * AnonymousRegistry constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        if ($factory->context()->isAnonymous()) {

            $listenerId = 'firewall.anonymous.default_authentication_listener.' . $factory->key();
            $providerId = 'firewall.anonymous.default_authentication_provider.' . $factory->key();
            $anonymousKey = $factory->context()->anonymousKey();

            $this->registerListener($listenerId, $anonymousKey);
            $this->registerProvider($providerId, $anonymousKey);

            $factory((new PayloadFactory())
                ->setProvider($providerId)
                ->setListener($listenerId)
            );
        }

        return $make($factory);
    }

    private function registerListener(string $listenerId, AnonymousKey $key): void
    {
        $this->container->bind($listenerId, function () use ($key) {
            return new AnonymousFirewall($key);
        });
    }

    private function registerProvider(string $providerId, AnonymousKey $key): void
    {
        $this->container->bind($providerId, function () use ($key) {
            return new AnonymousAuthenticationProvider($key);
        });
    }
}