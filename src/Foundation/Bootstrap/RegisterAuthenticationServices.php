<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Bootstrap;

use Thrust\Firewall\Factory\Factory;
use Thrust\Firewall\Factory\Payload\PayloadService;
use Thrust\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Thrust\Security\Foundation\Value\ProviderKey;

class RegisterAuthenticationServices
{
    /**
     * @var array
     */
    public static $positions = ['pre_auth', 'form', 'http', 'remember_me'];

    public function compose(Factory $factory, \Closure $make)
    {
        array_walk(self::$positions, function (string $position) use ($factory) {
            $factory
                ->make($position)
                ->map(function (AuthenticationServiceFactory $service) use ($factory) {
                         $factory($service->register($this->payload($service, $factory)));
                });
        });

        return $make($factory);
    }

    private function payload(AuthenticationServiceFactory $service, Factory $factory): PayloadService
    {
        return new PayloadService(
            new ProviderKey($factory->key()),
            $factory->context(),
            $factory->userProvider($service->userProviderKey()),
            $factory->defaultEntrypoint()
        );
    }
}