<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Bootstrap;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Thrust\Firewall\Factory\Factory;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Firewall\Foundation\Exception\AuthenticationExceptionHandler;
use Thrust\Firewall\Foundation\Exception\AuthorizationExceptionHandler;
use Thrust\Firewall\Foundation\Exception\ContextualSecurityHandler;
use Thrust\Firewall\Foundation\Exception\SecurityExceptionHandler;
use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Http\Response\AuthorizationDenied;
use Thrust\Security\Contract\Http\Response\Entrypoint;
use Thrust\Security\Foundation\Value\ProviderKey;

class FirewallExceptionHandler
{
    /**
     * @var Container
     */
    private $container;

    /**
     * FirewallExceptionHandler constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        $exceptionId = $this->registerExceptionListener($factory);

        $factory((new PayloadFactory())->setListener($exceptionId));

        $this->whenResolvingException(
            $exceptionId, $factory->aggregate()->entrypoints(), $factory->defaultEntrypoint());

        return $make($factory);
    }

    private function registerExceptionListener(Factory $factory): string
    {
        $id = 'firewall.exception_listener.' . $factory->key();

        $this->container->bind($id, function (Application $app) use ($factory) {
            return new SecurityExceptionHandler(
                $this->contextualExceptionInstance($factory),
                $app->make(AuthenticationExceptionHandler::class),
                $app->make(AuthorizationExceptionHandler::class)
            );
        });

        return $id;
    }

    private function contextualExceptionInstance(Factory $factory): ContextualSecurityHandler
    {
        return new ContextualSecurityHandler(
            new ProviderKey($factory->key()),
            $factory->context()->isStateless(),
            $this->container->make(TrustResolver::class),
            $this->resolveDefaultEntrypoint($factory->defaultEntrypoint()),
            $this->resolveDeniedHandler($factory->context()->deniedHandler())
        );
    }

    private function resolveDefaultEntrypoint(string $defaultEntrypoint = null): ?Entrypoint
    {
        if ($defaultEntrypoint) {
            return $this->container->make($defaultEntrypoint);
        }

        return null;
    }

    private function resolveDeniedHandler(string $deniedHandler = null): ? AuthorizationDenied
    {
        if ($deniedHandler) {
            return $this->container->make($deniedHandler);
        }

        return null;
    }

    private function whenResolvingException(string $exceptionId, array $entrypoints, string $defaultEntrypoint = null): void
    {
        if (!$entrypoints) {
            return;
        }

        foreach ($entrypoints as $entrypoint) {
            if ($entrypoint !== $defaultEntrypoint) {
                $this->container->resolving($entrypoint, function (Entrypoint $entrypoint, Application $app) use ($exceptionId) {
                    $app->make($exceptionId)->setEntrypoint($entrypoint);
                });
            }
        }
    }
}