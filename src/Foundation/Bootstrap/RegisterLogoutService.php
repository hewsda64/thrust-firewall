<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Bootstrap;

use Thrust\Firewall\Factory\Factory;

class RegisterLogoutService
{
    public function compose(Factory $factory, \Closure $make)
    {
        return $make($factory);
    }
}