<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Contracts\Strategy;

use Illuminate\Http\Request;

interface FirewallStrategy
{
    public function delegateHandling(Request $request): void;
}