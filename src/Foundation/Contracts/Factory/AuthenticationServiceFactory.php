<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Contracts\Factory;

use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Firewall\Factory\Payload\PayloadService;

interface AuthenticationServiceFactory
{
    public function register(PayloadService $payload): PayloadFactory;

    public function position(): string;

    public function key(): string;

    public function userProviderKey(): ?string;

    public function matcher(): ?RequestMatcherInterface;
}