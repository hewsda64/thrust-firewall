<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrust\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory as AuthenticationFactory;

abstract class AuthenticationServiceFactory implements AuthenticationFactory
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * AuthenticationServiceFactory constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function userProviderKey(): ?string
    {
        return null;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return null;
    }
}