<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Thrust\Firewall\Factory\Manager\Recaller\RecallerManager;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Firewall\Factory\Payload\PayloadService;
use Thrust\Security\Authentication\Provider\GenericFormAuthenticationProvider;
use Thrust\Security\Contract\User\UserChecker;
use Thrust\Security\Response\Authentication\DefaultAuthenticationFailure;
use Thrust\Security\Response\Authentication\DefaultSuccessAuthentication;

abstract class FormAuthenticationFactory extends AuthenticationServiceFactory
{
    /**
     * @var RecallerManager
     */
    private $recallerManager;

    /**
     * FormAuthenticationFactory constructor.
     *
     * @param Container $container
     * @param RecallerManager $recallerManager
     */
    public function __construct(Container $container, RecallerManager $recallerManager)
    {
        parent::__construct($container);

        $this->recallerManager = $recallerManager;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        $listenerId = $this->registerListener($payload);

        if( $payload->context->hasRecallerForService($this->key())){
            $this->recallerManager->create($payload, $listenerId, $this->key());
        }

        return (new PayloadFactory())
            ->setListener($listenerId)
            ->setProvider($this->registerProvider($payload));
    }

    public function registerProvider(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_provider.' . $payload->firewallKey->getKey();

        $this->container->bindIf($id, function (Application $app) use ($payload) {
            return new GenericFormAuthenticationProvider(
                $payload->firewallKey,
                $app->make($payload->userProviderId),
                $app->make(UserChecker::class),
                $app->make('hash'),
                true
            );
        });

        return $id;
    }

    abstract public function registerListener(PayloadService $payload): string;

    public function position(): string
    {
        return 'form';
    }

    protected function registerAuthenticationSuccess(string $route): string
    {
        $id = 'firewall.' . $this->key() . '.authentication_success_handler';

        $this->container->bindIf($id, function (Application $app) use ($route) {
            return new DefaultSuccessAuthentication(
                $app->make(ResponseFactory::class),
                $route
            );
        });

        return $id;
    }

    protected function registerAuthenticationFailure(string $route): string
    {
        $id = 'firewall.' . $this->key() . '.authentication_failure_handler';

        $this->container->bindIf($id, function (Application $app) use ($route) {
            return new DefaultAuthenticationFailure(
                $app->make(ResponseFactory::class),
                $route
            );
        });

        return $id;
    }
}