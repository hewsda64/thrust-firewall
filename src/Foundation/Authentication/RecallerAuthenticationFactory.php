<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrust\Firewall\Exception\FirewallException;
use Thrust\Firewall\Factory\Manager\Recaller\RecallerManager;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Firewall\Factory\Payload\PayloadService;
use Thrust\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Foundation\Value\RecallerKey;

abstract class RecallerAuthenticationFactory implements AuthenticationServiceFactory
{

    /**
     * @var RecallerManager
     */
    protected $recallerManager;

    /**
     * @var Container
     */
    protected $container;

    /**
     * AbstractRecallerAuthenticationFactory constructor.
     *
     * @param RecallerManager $recallerManager
     * @param Container $container
     */
    public function __construct(RecallerManager $recallerManager, Container $container)
    {
        $this->recallerManager = $recallerManager;
        $this->container = $container;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        $recallerKey = $payload->context->recaller($this->serviceKey());

        if (!$recallerKey) {
            throw new FirewallException('A recaller key is mandatory');
        }

        if (!$this->recallerManager->hasService($recallerKey)) {
            throw new FirewallException(
                sprintf('Recaller service with key %s not found', $recallerKey)
            );
        }

        $recallerId = $this->recallerManager->getServiceId($recallerKey);

        return (new PayloadFactory())
            ->setListener($this->registerListener($recallerId))
            ->setProvider($this->registerProvider($payload->firewallKey, $recallerKey));
    }

    abstract public function registerListener(string $recallerServiceId): string;

    abstract public function registerProvider(ProviderKey $providerKey, RecallerKey $recallerKey): string;

    abstract public function serviceKey(): string;

    public function position(): string
    {
        return 'remember_me';
    }

    public function userProviderKey(): ?string
    {
        return null;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return null;
    }
}