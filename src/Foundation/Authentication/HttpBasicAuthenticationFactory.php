<?php

namespace Thrust\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Firewall\Factory\Payload\PayloadService;
use Thrust\Firewall\Foundation\Http\Request\HttpBasicAuthenticationRequest;
use Thrust\Security\Authentication\Provider\GenericFormAuthenticationProvider;
use Thrust\Security\Contract\User\UserChecker;

abstract class HttpBasicAuthenticationFactory extends AuthenticationServiceFactory
{

    public function register(PayloadService $payload): PayloadFactory
    {
        $entryPointId = $this->registerEntrypoint($payload);

        return (new PayloadFactory())
            ->setListener($this->registerListener($payload, $entryPointId))
            ->setProvider($this->registerProvider($payload))
            ->setEntrypoint($entryPointId);
    }

    protected function registerProvider(PayloadService $payload): string
    {
        $providerId = 'firewall.' . $this->key() . '_authentication_provider.' . $payload->firewallKey->getKey();

        $this->container->bind($providerId, function (Application $app) use ($payload) {
            return new GenericFormAuthenticationProvider(
                $payload->firewallKey,
                $app->make($payload->userProviderId),
                $app->make(UserChecker::class),
                $app->make('hash')
            );
        });

        return $providerId;
    }

    abstract public function registerListener(PayloadService $payload, string $entrypointId): string;

    abstract public function registerEntrypoint(PayloadService $payload): string;

    public function position(): string
    {
        return 'http';
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new HttpBasicAuthenticationRequest();
    }
}