<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Exception;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Exception\AuthorizationException;
use Thrust\Security\Exception\InsufficientAuthentication;
use Thrust\Security\Foundation\Guard;

class AuthorizationExceptionHandler
{
    use Guard;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * AuthorizationExceptionHandler constructor.
     *
     * @param TrustResolver $trustResolver
     */
    public function __construct(TrustResolver $trustResolver)
    {
        $this->trustResolver = $trustResolver;
    }

    public function handle(AuthorizationException $exception, Request $request, ContextualSecurityHandler $contextualException): Response
    {
        // With default config, we added the recaller condition to avoid loop
        if (!$this->trustResolver->isFullFledged($this->token()) &&
            !$this->trustResolver->isRememberMe($this->token())
        ) {
            return $this->whenUserIsNotFullyAuthenticated($request, $exception, $contextualException);
        }

        return $this->whenUserIsNotGranted($request, $exception, $contextualException);
    }

    protected function whenUserIsNotFullyAuthenticated(Request $request, AuthorizationException $exception, ContextualSecurityHandler $contextual): Response
    {
        try {
            $message = 'Full authentication is required to access this resource.';
            $authException = new InsufficientAuthentication($message, 0, $exception);

            return $contextual->startAuthentication($request, $authException);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function whenUserIsNotGranted(Request $request, AuthorizationException $exception, ContextualSecurityHandler $contextual): Response
    {
        if (null !== $denied = $contextual->deniedHandler()) {
            return $denied->handle($request, $exception);
        }

        throw $exception;
    }
}