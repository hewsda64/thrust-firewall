<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Exception;

use Symfony\Component\HttpFoundation\Response;
use Thrust\Firewall\Foundation\Contracts\Exception\ExceptionRegistry;
use Thrust\Security\Contract\Exception\SecurityException;

trait SecurityExceptionHandlerTrait
{
    /**
     * @var ExceptionRegistry|SecurityExceptionHandler
     */
    protected $handler;

    public function render($request, \Exception $exception): Response
    {
        if ($exception instanceof SecurityException) {
            return $this->handler->handle($request, $exception);
        }

        return parent::render($request, $exception);
    }

    public function setFirewallExceptionHandler(ExceptionRegistry $handler): void
    {
        $this->handler = $handler;
    }
}