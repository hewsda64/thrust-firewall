<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Exception;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;

class AuthenticationExceptionHandler
{
    public function handle(AuthenticationException $exception, Request $request, ContextualSecurityHandler $contextualException): Response
    {
        try {
            return $contextualException->startAuthentication($request, $exception);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}