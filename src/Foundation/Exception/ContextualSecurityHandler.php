<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Exception;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Response\AuthorizationDenied;
use Thrust\Security\Contract\Http\Response\Entrypoint;
use Thrust\Security\Exception\UserStatusException;
use Thrust\Security\Foundation\Guard;
use Thrust\Security\Foundation\Value\ProviderKey;

class ContextualSecurityHandler
{
    use Guard;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var bool
     */
    private $stateless;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var Entrypoint
     */
    private $entrypoint;

    /**
     * @var AuthorizationDenied
     */
    private $authorizationDenied;

    /**
     * SecurityExceptionHandler constructor.
     *
     * @param ProviderKey $providerKey
     * @param bool $stateless
     * @param TrustResolver $trustResolver
     * @param Entrypoint|null $entrypoint
     * @param AuthorizationDenied|null $authorizationDenied
     */
    public function __construct(ProviderKey $providerKey,
                                bool $stateless,
                                TrustResolver $trustResolver,
                                Entrypoint $entrypoint = null,
                                AuthorizationDenied $authorizationDenied = null)
    {
        $this->providerKey = $providerKey;
        $this->stateless = $stateless;
        $this->trustResolver = $trustResolver;
        $this->entrypoint = $entrypoint;
        $this->authorizationDenied = $authorizationDenied;
    }

    public function startAuthentication(Request $request, AuthenticationException $exception): Response
    {
        if (!$this->entrypoint) {
            throw $exception;
        }

        if ($exception instanceof UserStatusException) {
            $this->eraseStorage();
        }

        return $this->entrypoint->start($request, $exception);
    }

    public function setEntrypoint(Entrypoint $entrypoint): ContextualSecurityHandler
    {
        $this->entrypoint = $entrypoint;

        return $this;
    }

    public function entrypointHandler(): ?Entrypoint
    {
        return $this->entrypoint;
    }

    public function setAuthorizationDenied(AuthorizationDenied $authorizationDenied): ContextualSecurityHandler
    {
        $this->authorizationDenied = $authorizationDenied;

        return $this;
    }

    public function deniedHandler(): ?AuthorizationDenied
    {
        return $this->authorizationDenied;
    }
}