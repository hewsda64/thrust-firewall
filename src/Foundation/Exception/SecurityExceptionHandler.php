<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Exception;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Firewall\Foundation\Contracts\Exception\ExceptionRegistry;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Exception\AuthorizationException;
use Thrust\Security\Contract\Exception\SecurityException;
use Thrust\Security\Contract\Http\Response\Entrypoint;

class SecurityExceptionHandler implements ExceptionRegistry
{

    /**
     * @var ContextualSecurityHandler
     */
    private $contextualException;

    /**
     * @var AuthenticationExceptionHandler
     */
    private $authenticationExceptionHandler;

    /**
     * @var AuthorizationExceptionHandler
     */
    private $authorizationExceptionHandler;

    /**
     * SecurityExceptionHandler constructor.
     *
     * @param ContextualSecurityHandler $contextualException
     * @param AuthenticationExceptionHandler $authenticationExceptionHandler
     * @param AuthorizationExceptionHandler $authorizationExceptionHandler
     */
    public function __construct(ContextualSecurityHandler $contextualException, AuthenticationExceptionHandler $authenticationExceptionHandler, AuthorizationExceptionHandler $authorizationExceptionHandler)
    {
        $this->contextualException = $contextualException;
        $this->authenticationExceptionHandler = $authenticationExceptionHandler;
        $this->authorizationExceptionHandler = $authorizationExceptionHandler;
    }

    public function handle(Request $request, SecurityException $securityException): Response
    {
        if ($securityException instanceof AuthenticationException) {
            return $this->authenticationExceptionHandler->handle($securityException, $request, $this->contextualException);
        }

        if ($securityException instanceof AuthorizationException) {
            return $this->authorizationExceptionHandler->handle($securityException, $request, $this->contextualException);
        }

        throw $securityException;
    }

    public function setEntrypoint(Entrypoint $entrypoint): void
    {
        $this->contextualException->setEntrypoint($entrypoint);
    }
}