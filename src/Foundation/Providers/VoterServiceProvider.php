<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Providers;

use Thrust\Firewall\Foundation\Providers\BaseProvider\BaseVoterServiceProvider;

class VoterServiceProvider extends BaseVoterServiceProvider
{
}