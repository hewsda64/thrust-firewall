<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Providers;

use Illuminate\Support\ServiceProvider;
use Thrust\Security\Authentication\Authentication;
use Thrust\Security\Authentication\AuthenticationProviders;
use Thrust\Security\Authentication\DefaultTrustResolver;
use Thrust\Security\Authentication\Token\AnonymousToken;
use Thrust\Security\Authentication\Token\RecallerToken;
use Thrust\Security\Authentication\Token\Storage\TokenStorage;
use Thrust\Security\Contract\Authentication\Authenticatable;
use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Token\Storage\TokenStorage as Storage;

class SecurityServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function register(): void
    {
        $this->app->singleton(Storage::class, TokenStorage::class);

        $this->app->bind(TrustResolver::class, function () {
            return new DefaultTrustResolver(AnonymousToken::class, RecallerToken::class);
        });

        $this->app->singleton(AuthenticationProviders::class);

        $this->app->singleton(Authenticatable::class, Authentication::class);
    }

    public function provides(): array
    {
        return [
            Storage::class,
            TrustResolver::class,
            AuthenticationProviders::class,
            Authenticatable::class
        ];
    }
}