<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Providers\BaseProvider;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Thrust\Security\Authorization\Voter\AnonymousVoter;
use Thrust\Security\Authorization\Voter\AuthenticatedTokenVoter;
use Thrust\Security\Authorization\Voter\RoleHierarchyVoter;
use Thrust\Security\Contract\Role\RoleHierarchy;

class BaseVoterServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $voters = [
        AuthenticatedTokenVoter::class,
        AnonymousVoter::class,
        RoleHierarchyVoter::class,
    ];

    public function register(): void
    {
        $this->registerVoters();

        $this->tagVoters();
    }

    protected function tagVoters(): void
    {
        $this->app->tag($this->voters, config('firewall.authorization.voters_tag'));
    }

    protected function registerVoters(): void
    {
        $this->app->bindIf(RoleHierarchyVoter::class, function (Application $app) {
            return new RoleHierarchyVoter(
                $app->make(RoleHierarchy::class),
                config('firewall.authorization.role_prefix')
            );
        });
    }
}