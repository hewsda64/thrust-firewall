<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Thrust\Firewall\Factory\Config\ConfigChecker;
use Thrust\Firewall\Factory\Manager\AuthenticationManager;
use Thrust\Firewall\Factory\Manager\Recaller\RecallerManager;
use Thrust\Firewall\Factory\Registry;
use Thrust\Firewall\Foundation\Contracts\Strategy\FirewallStrategy;
use Thrust\Firewall\Foundation\Http\Middleware\Firewall;
use Thrust\Firewall\Foundation\Http\Middleware\SessionContextAware;

class FirewallServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->registerBootstraps();

        $this->registerMiddleware();

        $this->registerManager();
    }

    protected function registerBootstraps(): void
    {
        $this->app->singleton(Registry::class, function () {
            return new Registry(config('firewall.bootstraps'));
        });
    }

    protected function registerMiddleware(): void
    {
        $this->app->singleton(SessionContextAware::class);

        $this->app->bindIf(FirewallStrategy::class, config('firewall.strategy'));

        $this->app->singleton(Firewall::class);
    }

    protected function registerManager(): void
    {
        $this->app->singleton(RecallerManager::class);

        $this->app->singleton(AuthenticationManager::class, function (Application $app) {
            return new AuthenticationManager(
                new ConfigChecker(config('firewall.authentication')),
                $app
            );
        });
    }
}