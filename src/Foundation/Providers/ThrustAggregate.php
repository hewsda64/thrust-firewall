<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Providers;

use Illuminate\Support\AggregateServiceProvider;

class ThrustAggregate extends AggregateServiceProvider
{
    /**
     * @var array
     */
    protected $providers = [
        BladeServiceProvider::class,

        ApplicationServiceProvider::class,

        VoterServiceProvider::class,

        AuthorizationServiceProvider::class,

        SecurityServiceProvider::class,

        FirewallServiceProvider::class,
    ];
}