<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Thrust\Security\Contract\Authorization\Authorizable;
use Thrust\Security\Contract\Authorization\Grantable;
use Thrust\Security\Contract\Role\RoleHierarchy as RoleHierarchyContract;

class AuthorizationServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var string
     */
    private $namespace = 'firewall.authorization';

    public function register(): void
    {
        $this->registerAuthorizationChecker();

        $this->registerRoleHierarchy();

        $this->registerAuthorizationStrategy();
    }

    private function registerAuthorizationChecker(): void
    {
        $this->app->bindIf(Grantable::class, config($this->namespace . '.grant'));
    }

    private function registerRoleHierarchy(): void
    {
        $this->app->bindIf(RoleHierarchyContract::class, function () {
            $class = config($this->namespace . '.role_hierarchy.service');
            $roles = config($this->namespace . '.role_hierarchy.roles');

            return new $class($roles);
        });
    }

    private function registerAuthorizationStrategy(): void
    {
        $this->app->bindIf(Authorizable::class, function (Application $app) {
            $strategy = config($this->namespace . '.strategy');
            $tag = config($this->namespace . '.voters_tag');

            return new $strategy($app->tagged($tag));
        });
    }

    public function provides(): array
    {
        return [
            Grantable::class,
            RoleHierarchyContract::class,
            Authorizable::class
        ];
    }
}