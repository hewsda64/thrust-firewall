<?php

declare(strict_types=1);

namespace Thrust\Firewall\Foundation\Providers;

use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        \Blade::directive('isGranted', function ($expression) {
            return "<?php if (app(\Thrust\\Security\\Contract\\Authorization\\Grantable::class)->isGranted({$expression})): ?>";
        });

        \Blade::directive('isNotGranted', function ($expression) {
            return "<?php if (!app(\Thrust\\Security\\Contract\\Authorization\\Grantable::class)->isGranted({$expression})): ?>";
        });
    }
}