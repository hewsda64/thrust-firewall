<?php

declare(strict_types=1);

namespace Thrust\Firewall\Strategy;

use Illuminate\Http\Request;

class GlobalStackFirewallStrategy extends FirewallStrategy
{
    public function delegateHandling(Request $request): void
    {
        $this->processFirewall($request, []);
    }
}