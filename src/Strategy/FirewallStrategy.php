<?php

declare(strict_types=1);

namespace Thrust\Firewall\Strategy;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Thrust\Firewall\Exception\FirewallException;
use Thrust\Firewall\Foundation\Contracts\Exception\ExceptionRegistry;
use Thrust\Firewall\Foundation\Contracts\Strategy\FirewallStrategy as Strategy;
use Thrust\Firewall\Foundation\Http\Event\FirewallHandled;
use Thrust\Firewall\Manager;

abstract class FirewallStrategy implements Strategy
{

    /**
     * @var Manager
     */
    private $manager;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Dispatcher
     */
    protected $eventDispatcher;

    /**
     * FirewallStrategy constructor.
     *
     * @param Manager $manager
     * @param Router $router
     * @param Dispatcher $dispatcher
     * @param Container $container
     */
    public function __construct(Manager $manager, Router $router, Dispatcher $dispatcher, Container $container)
    {
        $this->manager = $manager;
        $this->router = $router;
        $this->eventDispatcher = $dispatcher;
        $this->container = $container;
    }

    protected function processFirewall(Request $request, array $middleware): void
    {
        $this->manager->raise($middleware, $request)
            ->each(function (array $middleware, string $firewallName) {
                $this->resolveExceptionHandler(array_pop($middleware), $firewallName);

                $this->setMiddlewareGroupToRouter($middleware, $firewallName);
            });
    }

    private function setMiddlewareGroupToRouter(array $middleware, string $middlewareGroup): void
    {
        $this->router->middlewareGroup($middlewareGroup, $middleware); // no merge

        $this->eventDispatcher->dispatch(new FirewallHandled($middlewareGroup, $middleware));
    }

    private function resolveExceptionHandler(string $exceptionHandler, string $firewallName): void
    {
        $exceptionHandler = $this->container->make($exceptionHandler);

        if (!$exceptionHandler instanceof ExceptionRegistry) {
            throw new FirewallException(
                sprintf('Last middleware in firewall %s must implement %s',
                    $firewallName, ExceptionRegistry::class)
            );
        }

        $this->container->make(ExceptionHandler::class)
            ->setFirewallExceptionHandler($exceptionHandler);
    }
}