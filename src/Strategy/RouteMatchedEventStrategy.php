<?php

declare(strict_types=1);

namespace Thrust\Firewall\Strategy;

use Illuminate\Http\Request;
use Illuminate\Routing\Events\RouteMatched;

class RouteMatchedEventStrategy extends FirewallStrategy
{
    public function onEvent(RouteMatched $event): void
    {
        $this->processFirewall($event->request, $event->route->middleware());
    }

    public function delegateHandling(Request $request): void
    {
        $this->eventDispatcher->listen(RouteMatched::class, [$this, 'OnEvent']);
    }
}