<?php

declare(strict_types=1);

namespace Thrust\Firewall;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Thrust\Firewall\Factory\Manager\AuthenticationManager;

class Manager
{
    /**
     * @var AuthenticationManager
     */
    private $authenticationManager;

    /**
     * @var Director
     */
    private $director;

    /**
     * @var Collection
     */
    protected $services;

    /**
     * Manager constructor.
     *
     * @param AuthenticationManager $authenticationManager
     * @param Director $director
     */
    public function __construct(AuthenticationManager $authenticationManager, Director $director)
    {
        $this->authenticationManager = $authenticationManager;
        $this->director = $director;
        $this->services = new Collection();
    }

    public function raise(array $middleware, Request $request): Collection
    {
        foreach ($this->filterFirewallMiddleware($middleware) as $middlewareName) {
            $this->services->put($middlewareName, $this->authenticationManager->create($middlewareName));
        }

        return $this->director->process($this->services, $request);
    }

    protected function filterFirewallMiddleware(array $middleware): array
    {
        return array_filter($middleware, function (string $name) {
            return $this->authenticationManager->hasFirewall($name);
        });
    }
}