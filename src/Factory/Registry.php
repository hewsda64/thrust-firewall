<?php

declare(strict_types=1);

namespace Thrust\Firewall\Factory;

use Thrust\Firewall\Exception\FirewallException;

class Registry
{
    /**
     * @var array
     */
    protected $bootstraps = [];

    /**
     * @var int
     */
    protected $step = 10;

    /**
     * FirewallRegistry constructor.
     *
     * @param array $bootstraps
     */
    public function __construct(array $bootstraps = [])
    {
        foreach ($bootstraps as $bootstrap) {
            $this->bootstraps[] = $this->checkRegistry($bootstrap);
        }
    }

    public function append(string $bootstrap, int $priority): void
    {
        $this->bootstraps[] = $this->checkRegistry([$bootstrap, $priority]);
    }

    public function getHighestPriority(): int
    {
        $priority = 0;
        foreach ($this->bootstraps as [$service, $prioritized]) {
            if ($prioritized > $priority) {
                $priority = $prioritized;
            }
        }

        return $priority;
    }

    final public function bootstraps(): array
    {
        $this->orderByPriority();

        return array_map(function (array $bootstrap) {
            return array_shift($bootstrap);
        }, $this->bootstraps);
    }

    protected function checkRegistry($bootstrap): array
    {
        if (is_string($bootstrap)) {
            $bootstrap = $this->prioritize($bootstrap);
        }

        if (2 !== count($bootstrap)) {
            throw new FirewallException('Bootstrap service must be an array of two members with service id and priority');
        }

        [$serviceId, $priority] = $bootstrap;

        if (!$this->isUnique($serviceId)) {
            throw new FirewallException(
                sprintf('Firewall bootstrap service "%s" already exists.', $serviceId));
        }

        if (!is_int($priority)) {
            throw new FirewallException(sprintf(
                'No priority set to service bootstrap "%s"', $serviceId));
        }

        return $bootstrap;
    }

    public function isUnique(string $serviceId): bool
    {
        foreach ($this->bootstraps as [$id, $priority]) {
            if ($serviceId === $id) {
                return false;
            }
        }

        return true;
    }

    protected function prioritize(string $bootstrap): array
    {
        return [$bootstrap, $this->getHighestPriority() + $this->step];
    }

    protected function orderByPriority(): void
    {
        usort($this->bootstraps, function (array $first, array $next) {
            return array_pop($first) <=> array_pop($next);
        });
    }
}