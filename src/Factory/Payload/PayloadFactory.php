<?php

declare(strict_types=1);

namespace Thrust\Firewall\Factory\Payload;

class PayloadFactory
{
    /**
     * @var string
     */
    private $listener;

    /**
     * @var string
     */
    private $provider;

    /**
     * @var string
     */
    private $entrypoint;

    public function setListener(string $listener): PayloadFactory
    {
        $this->listener = $listener;

        return $this;
    }

    public function setProvider(string $provider): PayloadFactory
    {
        $this->provider = $provider;

        return $this;
    }

    public function setEntrypoint(string $entrypoint): PayloadFactory
    {
        $this->entrypoint = $entrypoint;

        return $this;
    }

    public function listener(): ?string
    {
        return $this->listener;
    }

    public function provider(): ?string
    {
        return $this->provider;
    }

    public function entrypoint(): ?string
    {
        return $this->entrypoint;
    }
}