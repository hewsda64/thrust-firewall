<?php

declare(strict_types=1);

namespace Thrust\Firewall\Factory;

use Illuminate\Support\Collection;
use Thrust\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;

class AuthenticationServices
{
    /**
     * @var Collection
     */
    private $services;

    /**
     * AuthenticationServices constructor.
     *
     * @param array|null $services
     */
    public function __construct(array $services = null)
    {
        $this->services = new Collection($services ?? []);
    }

    public function add(AuthenticationServiceFactory $service): AuthenticationServices
    {
        $this->services->push($service);

        return $this;
    }

    final public function all(): Collection
    {
        return $this->services;
    }
}