<?php

declare(strict_types=1);

namespace Thrust\Firewall\Factory\Config;

use Thrust\Firewall\Exception\FirewallException;

class ConfigChecker
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var string
     */
    private $namespace = 'firewall';

    /**
     * FirewallAware constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getFirewall(string $name): array
    {
        $firewall = array_get($this->config, $this->namespace . '.' . $name);

        if (null === $firewall) {
            throw new FirewallException(sprintf('Can not locate %s key in firewall configuration.', $name));
        }

        $this->validateFirewallConfiguration($firewall);

        return $firewall;
    }

    public function getUserProviders(): array
    {
        if (!isset($this->config['user_providers'])) {
            throw new FirewallException('Missing "user_providers" key in firewall configuration');
        }

        $providers = $this->config['user_providers'];

        if (!is_array($providers) || empty($providers)) {
            throw new FirewallException('User providers must be an array and cannot be empty');
        }

        return $providers;
    }

    public function hasFirewall(string $middleware): bool
    {
        return null !== array_get($this->config, $this->namespace . '.' . $middleware);
    }

    protected function validateFirewallConfiguration($firewall): void
    {
        if (!is_array($firewall)) {
            throw new FirewallException('Firewall service configuration must be an array.');
        }

        if (!isset($firewall['context'])) {
            throw new FirewallException('Missing "context" key in firewall configuration.');
        }

        if (!isset($firewall['services'])) {
            throw new FirewallException('Missing "services" key in firewall configuration.');
        }

        if (!is_array($firewall['services']) || empty($firewall['services'])) {
            throw new FirewallException(
                '"services" key from firewall configuration must be an array and must not be empty.');
        }
    }
}