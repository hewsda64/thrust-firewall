<?php

declare(strict_types=1);

namespace Thrust\Firewall\Factory\Manager\Recaller;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Contracts\Foundation\Application;
use Thrust\Firewall\Exception\FirewallException;
use Thrust\Firewall\Factory\Payload\PayloadService;
use Thrust\Security\Foundation\Value\RecallerKey;
use Thrust\Security\Service\Recaller\SimpleRecallerService;

class RecallerManager
{
    /**
     * @var array
     */
    private $services = [];

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $factories = [];

    /**
     * RecallerManager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function create(PayloadService $payload, string $listenerId, string $serviceKey): string
    {
        if (!$payload->context->hasRecallerForService($serviceKey)) {
            throw new FirewallException(
                sprintf('Recaller manager can not find any context for service key "%s" in configuration.', $serviceKey)
            );
        }

        $recallerKey = $payload->context->recaller($serviceKey);

        if ($this->hasService($recallerKey)) {
            return $this->services[$recallerKey->getKey()];
        }

        if (isset($this->factories[$serviceKey])) {
            $service = $this->factories[$serviceKey];
            return $this->services[$recallerKey->getKey()] = $this->registerExtender($payload, $listenerId, $serviceKey, $service);
        }

        return $this->services[$recallerKey->getKey()] = $this->createSimpleService($payload, $listenerId, $serviceKey);
    }

    protected function registerExtender(PayloadService $payload, string $listenerId, string $serviceKey, callable $service): string
    {
        $serviceId = $service($payload, $serviceKey);

        $this->setServiceOnResolving($listenerId, $serviceId);

        return $serviceId;
    }

    protected function createSimpleService(PayloadService $payload, string $listenerId, string $serviceKey): string
    {
        $recallerKey = $payload->context->recaller($serviceKey);
        $serviceId = 'firewall.remember_me.recaller_' . $recallerKey->getKey() . '_service.';

        $this->container->bind($serviceId, function (Application $app) use ($payload, $recallerKey) {
            return new SimpleRecallerService(
                $recallerKey,
                $payload->firewallKey,
                $app->make(QueueingFactory::class),
                $app->make($payload->userProviderId)
            );
        });

        $this->setServiceOnResolving($listenerId, $serviceId);

        return $serviceId;
    }

    protected function setServiceOnResolving(string $listenerId, $serviceId): void
    {
        $this->container->resolving($listenerId,
            function ($listener, Application $app) use ($serviceId) {
                if (!method_exists($listener, 'setRecaller')) {
                    throw new FirewallException(
                        sprintf('Class %s must implement "setRecaller" method.', get_class($listener))
                    );
                }

                $listener->setRecaller($app->make($serviceId));
            });
    }

    public function hasService(RecallerKey $recallerKey): bool
    {
        return array_key_exists($recallerKey->getKey(), $this->services);
    }

    public function getServiceId(RecallerKey $recallerKey): string
    {
        if ($this->hasService($recallerKey)) {
            return $this->services[$recallerKey->getKey()];
        }

        throw new FirewallException(
            sprintf('Recaller service with key %s not found in recaller manager.',
                $recallerKey->getKey())
        );
    }

    public function extend(string $serviceKey, callable $service): void
    {
        $this->factories[$serviceKey] = $service;
    }
}