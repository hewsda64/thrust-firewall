<?php

declare(strict_types=1);

namespace Thrust\Firewall\Factory\Manager;

use Illuminate\Contracts\Container\Container;
use Thrust\Firewall\Exception\FirewallException;
use Thrust\Firewall\Factory\AuthenticationServices;
use Thrust\Firewall\Factory\Config\ConfigChecker;
use Thrust\Firewall\Factory\Factory;
use Thrust\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Thrust\Security\Foundation\Value\ContextKey;

class AuthenticationManager
{
    /**
     * @var ConfigChecker
     */
    private $configChecker;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    protected $firewall = [];

    /**
     * @var array
     */
    protected $factories = [];

    /**
     * AuthenticationManager constructor.
     *
     * @param ConfigChecker $configChecker
     * @param Container $container
     */
    public function __construct(ConfigChecker $configChecker, Container $container)
    {
        $this->configChecker = $configChecker;
        $this->container = $container;
    }

    public function create(string $name): Factory
    {
        if (isset($this->firewall[$name])) {
            return $this->firewall[$name];
        }

        if (!$this->hasFirewall($name)) {
            throw new FirewallException(
                sprintf('Firewall name %s does not exists.', $name));
        }

        $config = $this->configChecker->getFirewall($name);

        return $this->firewall[$name] = $this->getFactory($name, $config);
    }

    public function extend(string $firewallName, string $serviceAlias, callable $authenticationServiceFactory): AuthenticationManager
    {
        if (!$this->hasFirewall($firewallName)) {
            throw new FirewallException(
                sprintf('Firewall name %s does not exists.', $firewallName));
        }

        $this->factories[$firewallName][$serviceAlias] = $authenticationServiceFactory;

        return $this;
    }

    public function hasFirewall(string $name): bool
    {
        return $this->configChecker->hasFirewall($name);
    }

    protected function getFactory(string $firewallName, array $config): Factory
    {
        return new Factory(
            new ContextKey($firewallName),
            $this->resolveFactories($firewallName, $config),
            $this->resolveFirewallContext($config['context']),
            $this->configChecker->getUserProviders()
        );
    }

    protected function resolveFactories(string $middleware, array $config): AuthenticationServices
    {
        $factories = [];

        foreach ((array)$config['services'] as $key => $service) {
            $callback = array_get($this->factories, $middleware . '.' . $service);

            if ($callback) {
                $factories[] = is_callable($callback) ? $callback($this->container) : $this->container->make($callback);
            }
        }

        if (empty($factories)) {
            throw new FirewallException(
                sprintf('No service has been registered for firewall %s', $middleware));
        }

        return new AuthenticationServices($factories);
    }

    protected function resolveFirewallContext($context): FirewallContext
    {
        if (is_callable($context)) {
            return $context($this->container->make(FirewallContext::class));
        }

        return $this->container->make($context);
    }
}