<?php

declare(strict_types=1);

namespace Thrust\Firewall\Factory;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Thrust\Firewall\Exception\FirewallException;
use Thrust\Firewall\Factory\Payload\PayloadFactory;
use Thrust\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Thrust\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Thrust\Security\Foundation\Value\ContextKey;

class Factory
{

    /**
     * @var FirewallContext
     */
    private $context;

    /**
     * @var Aggregate
     */
    private $aggregate;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $defaultEntrypoint;

    /**
     * @var ContextKey
     */
    private $firewallKey;

    /**
     * @var AuthenticationServices
     */
    private $services;

    /**
     * @var array
     */
    private $userProviderIds;

    /**
     * Factory constructor.
     *
     * @param ContextKey $firewallKey
     * @param AuthenticationServices $services
     * @param FirewallContext $context
     * @param array $userProviderIds
     */
    public function __construct(ContextKey $firewallKey,
                                AuthenticationServices $services,
                                FirewallContext $context,
                                array $userProviderIds)
    {
        $this->context = $context;
        $this->firewallKey = $firewallKey;
        $this->services = $services;
        $this->userProviderIds = $userProviderIds;
        $this->aggregate = new Aggregate();
    }

    public function make(string $position): Collection
    {
        return $this->services->all()->filter(
            function (AuthenticationServiceFactory $serviceFactory) use ($position) {
                return $position === $serviceFactory->position();
            });
    }

    public function __invoke(PayloadFactory $payload): void
    {
        ($this->aggregate)($payload);
    }

    final public function middleware(): array
    {
        return $this->aggregate->listeners();
    }

    public function context(): FirewallContext
    {
        return $this->context;
    }

    public function aggregate(): Aggregate
    {
        return $this->aggregate;
    }

    public function key(): string
    {
        if (!$this->hasSameContext() && !$this->context()->isStateless()) {
            return $this->context->contextKey()->getKey();
        }

        return $this->firewallKey->getKey();
    }

    public function originalKey(): ContextKey
    {
        return $this->firewallKey;
    }

    public function hasSameContext(): bool
    {
        return $this->context->contextKey()->sameValueAs($this->firewallKey);
    }

    public function setRequest(Request $request): Factory
    {
        $this->request = $request;

        return $this;
    }

    public function request(): Request
    {
        return $this->request;
    }

    public function defaultEntrypoint(): ?string
    {
        return $this->defaultEntrypoint;
    }

    public function setDefaultEntrypoint(string $defaultEntrypoint): void
    {
        $this->defaultEntrypoint = $defaultEntrypoint;
    }

    public function userProviderIds(): array
    {
        return $this->userProviderIds;
    }

    public function userProvider(string $serviceUserProvider = null): string
    {
        $id = $serviceUserProvider ?? $this->context()->providerKey()->getKey();

        if (!array_key_exists($id, $this->userProviderIds)) {
            throw new FirewallException(
                sprintf('User provider with id "%s" not found', $id));
        }

        return $this->userProviderIds[$id];
    }
}