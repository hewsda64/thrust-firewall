<?php

declare(strict_types=1);

namespace Thrust\Firewall\Exception;

class FirewallException extends \RuntimeException
{
}