<?php

return [

    'strategy' => \Thrust\Firewall\Strategy\RouteMatchedEventStrategy::class,

    'bootstraps' => [
        \Thrust\Firewall\Foundation\Bootstrap\AppBootstrap::class,
        \Thrust\Firewall\Foundation\Bootstrap\AuthenticationSerialization::class,
        \Thrust\Firewall\Foundation\Bootstrap\RegisterLogoutService::class,
        \Thrust\Firewall\Foundation\Bootstrap\RegisterAuthenticationServices::class,
        \Thrust\Firewall\Foundation\Bootstrap\AuthorizeAnonymousRequest::class,
        \Thrust\Firewall\Foundation\Bootstrap\FirewallExceptionHandler::class
    ],

    'authentication' => [

        'user_providers' => [
            // 'service_alias' => 'service_id'
        ],

        'firewall' => [

            /* 'frontend' => [
                 // 'context' => 'Some firewall context',
                 // 'services' => ['Authentication services factories']
             ],
            */
        ]
    ],

    'authorization' => [
        'voters_tag' => 'firewall.authorization.voters',

        'role_prefix' => 'ROLE_',

        'role_hierarchy' => [

            'service' => \Thrust\Security\Role\RoleHierarchy::class,
            'roles' => [ /*'ROLE_ADMIN' => ['ROLE_USER']*/]
        ],

        'strategy' => \Thrust\Security\Authorization\Strategy\UnanimousAuthorizationStrategy::class,

        'grant' => \Thrust\Security\Authorization\AuthorizationChecker::class,
    ],
];